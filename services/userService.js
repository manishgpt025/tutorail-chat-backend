const userModel = require('../models/userModel.js');

const createData = (bodyData, callback) => {
    userModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findOneData = (bodyData, options={}, callback) => {
    userModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
const getData = (bodyData, options={}, callback) => {
    userModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// update only one record
const updateOneData = (query, bodyData, options, callback) => {
    userModel.update(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// Get record by pagination
const getPaginateData = (data, options, callback) => {
    userModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}
// Get record by pagination
const getPaginateLimitData = (data, options, callback) => {
    userModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}
// find only one record async
const findOneAsync = async (query, options = {}) => {
    return await userModel.findOne(query, options).lean();
}
// find records async
const findAsync = async (query, options = {}) => {
    return await userModel.find(query, options).lean();
}

// Get Lat lang user data using aggregate
const getLatLangData = (callback) => {
    userModel.aggregate([
        {
            $group: {
               _id: '$country',
               country: { "$first": "$country" },
               lat: { "$first": "$lat" },
               lang: { "$first": "$lang" },
               count: { $sum: 1 }
            }
        },
        {
            "$project": {
                "_id":0,
                "country":1,
                "lat":1,
                "lang":1,
                "count":1
            }
        },
    ]).exec((err, result) => {
        callback(err, result);
    });
};

// Get all unique dob date using aggregate
const getAllDobDate = (callback) => {
    userModel.aggregate([
        {
            $group: {
               _id: '$dob',
               dob: { "$first": "$dob" },
            }
        },
        {
            "$project": {
                "_id":0,
                "date": { $dateToString: { format: "%m-%d", date: "$dob" } }
            }
        },
        {
            $group: {
               _id: "$date",
               "date": { $first: "$date" }
            }
        },
        {
            "$project": {
                "_id":0,
                "date": 1
            }
        }
    ]).exec((err, result) => {
        callback(err, result);
    });
};

// Get all unique dob date using aggregate
const getEqualDobDate = (bodyData, options,callback) => {
    var aggregate = userModel.aggregate([
        {
            '$match': {'_id': {'$ne': bodyData.user_id}}
        },
        {
            "$project": {
                "_id":1,
                "username":1,
                "dob":1,
                "profile_image":1,
                "country":1,
                "bio":1,
                "blocks":1,
                "hobbies":1,
                "date": { $dateToString: { format: "%m-%d", date: "$dob" } }
            }
        },
        {
            $match : { "date" : bodyData.dob }
        }
    ])
    userModel.aggregatePaginate(aggregate, options, (error, success, pages, total) => {
        callback(error, success, pages, total);
    })
};

// Delete record
const deleteRecord = (query, callback) => {
    userModel.deleteOne(query, (err, result) => {
        callback(err, result);
    });
}

module.exports = {
    createData,
    findOneData,
    getData,
    updateOneData,
    getPaginateData,
    getPaginateLimitData,
    findOneAsync,
    findAsync,
    getLatLangData,
    deleteRecord,
    getAllDobDate,
    getEqualDobDate
};