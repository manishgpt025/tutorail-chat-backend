
const questionModel = require('../models/questionModel.js');

const createData = (bodyData, callback) => {
    questionModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findOneData = (bodyData, options={}, callback) => {
    questionModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
const getData = (bodyData, options={}, callback) => {
    questionModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    }).sort({createdAt:1});
}
// update only one record
const updateOneData = (query, bodyData, options, callback) => {
    questionModel.update(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// Get record by pagination
const getPaginateData = (data, options, callback) => {
    questionModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}
// Get record by pagination
const getPaginateLimitData = (data, options, callback) => {
    questionModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}
// find only one record async
const findOneAsync = async (query, options = {}) => {
    return await questionModel.findOne(query, options).lean();
}
// find records async
const findAsync = async (query, options = {}) => {
    return await questionModel.find(query, options).lean();
}

// Delete record
const deleteRecord = (query, callback) => {
    questionModel.deleteOne(query, (err, result) => {
        callback(err, result);
    });
}

// Get chat list user data using aggregate
const getQuestionListData = (bodyData, callback) => {
    questionModel.aggregate([
        {
            $lookup: {
                from: "users",
                localField:  "user_id",
                foreignField: "_id",
                as: "user_data"
            }
        },
        {
            "$project":{
                "user_data":1,
                "subject":1,
                "file":1,
                "question":1,
                "createdAt":1
            }
        },
        { $sort: bodyData.sort }
    ]).exec((err, result) => {
        callback(err, result);
    });
};

module.exports = {
    createData,
    findOneData,
    getData,
    updateOneData,
    getPaginateData,
    getPaginateLimitData,
    findOneAsync,
    findAsync,
    deleteRecord,
    getQuestionListData
};