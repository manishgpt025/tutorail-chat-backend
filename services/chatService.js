const chatModel = require('../models/chatModel.js');

const createData = (bodyData, callback) => {
    chatModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findOneData = (bodyData, options={}, callback) => {
    chatModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
const getData = (bodyData, options={}, callback) => {
    chatModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    }).sort({createdAt:1});
}
// update only one record
const updateOneData = (query, bodyData, options, callback) => {
    chatModel.update(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// Get record by pagination
const getPaginateData = (data, options, callback) => {
    chatModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}
// Get record by pagination
const getPaginateLimitData = (data, options, callback) => {
    chatModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}
// find only one record async
const findOneAsync = async (query, options = {}) => {
    return await chatModel.findOne(query, options).lean();
}
// find records async
const findAsync = async (query, options = {}) => {
    return await chatModel.find(query, options).lean();
}

// Delete record
const deleteRecord = (query, callback) => {
    chatModel.deleteOne(query, (err, result) => {
        callback(err, result);
    });
}

module.exports = {
    createData,
    findOneData,
    getData,
    updateOneData,
    getPaginateData,
    getPaginateLimitData,
    findOneAsync,
    findAsync,
    deleteRecord
};