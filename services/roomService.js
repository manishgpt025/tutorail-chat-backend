const roomModel = require('../models/roomModel.js');
//const ObjectId = mongoose.Types.ObjectId;
const mongoose = require("mongoose");
//const ObjectId = require('mongodb').ObjectID;
const createData = (bodyData, callback) => {
    roomModel.create(bodyData, (err, result) => {
        callback(err, result);
    });
}
const findOneData = (bodyData, options={}, callback) => {
    roomModel.findOne(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
const getData = (bodyData, options={}, callback) => {
    roomModel.find(bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// update only one record
const updateOneData = (query, bodyData, options, callback) => {
    roomModel.update(query, bodyData, options, (err, result) => {
        callback(err, result);
    });
}
// Get record by pagination
const getPaginateData = (data, options, callback) => {
    roomModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}
// Get record by pagination
const getPaginateLimitData = (data, options, callback) => {
    roomModel.paginate(data, options, (err, result) => {
        callback(err, result);
    });
}
// find only one record async
const findOneAsync = async (query, options = {}) => {
    return await roomModel.findOne(query, options).lean();
}
// find records async
const findAsync = async (query, options = {}) => {
    return await roomModel.find(query, options).lean();
}

// Delete record
const deleteRecord = (query, callback) => {
    roomModel.deleteOne(query, (err, result) => {
        callback(err, result);
    });
}

// Get chat list user data using aggregate
const getChatListData = (bodyData, callback) => {
    console.log('adminIdf=====',bodyData.adminId)
    roomModel.aggregate([
        {
            "$match": {
                "$and":[
                    {"$or": [{sender_id: bodyData.adminId},{receiver_id: bodyData.adminId}]},
                    {status:true}
                ]
            }
        },
        // {
        //     "$lookup": {
        //     "from": "chats",
        //     "as": "chat_data",
        //     "let": { "chatId": "$room_id" },
        //     "pipeline": [
        //         { "$match":
        //             {
        //                 "$expr": { "$eq": [ "$$room_id", "$chatId" ] }
        //             }
        //         }
        //     ]
        //   }
        // },
        {
            $lookup: {
                from: "chats",
                localField:  "room_id",
                foreignField: "room_id",
                as: "chat_data"
            }
        },
        {
            "$addFields": {
                "chater_id": {
                  "$cond": {
                    "if": {
                      "$eq": [ "$sender_id", bodyData.adminId]
                    },
                    "then": "$receiver_id",
                    "else": "$sender_id"
                  }
                }
            }
        },

        {
            $lookup: {
                from: "users",
                localField:  "chater_id",
                foreignField: "_id",
                as: "user_data"
            }
        },
        {
            "$project":{
                "user_data":1,
                "chat_data":1
            }
        }

    ]).exec((err, result) => {
        console.log('qqqqqqqqqq',result)
        callback(err, result);
    });
};

module.exports = {
    createData,
    findOneData,
    getData,
    updateOneData,
    getPaginateData,
    getPaginateLimitData,
    findOneAsync,
    findAsync,
    deleteRecord,
    getChatListData
};