// Controllers
const userController = require("../controllers/userController");
// Express router
const router = require('express').Router();
/* Routes */
router.post('/login', userController.login);
router.post('/signUp', userController.signUp);
router.post('/chatHistory', userController.chatHistory);
router.get('/allUserList', userController.userList);
router.get('/adminDetail', userController.adminData);
router.post('/submitContactUs', userController.contactUs);
router.post('/submitCareer', userController.career);
router.post('/submitQuestion', userController.question);
router.get('/getQuestion', userController.getQuestion);

/*Export*/
module.exports = router;