const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let userModel = new schema({
    name: {
        type: String,// name of person
        trim: true,
    },
    email: {
        type: String,//email id of person
        trim: true
    },
    password: {
        type: String ,// password of person
        default: ""
    },
    role: {
        type: String ,// Role of person
        default: "user"
    },
},{
    timestamps: true
});

userModel.plugin(mongoosePaginate)
userModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('user', userModel);