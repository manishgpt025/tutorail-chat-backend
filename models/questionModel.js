const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let questionModel = new schema({
    subject: {
        type: String// subject for query
    },
    user_id: {
        type: schema.Types.ObjectId,  //user info
        ref:"user"
    },
    question: {
        type: String, // message of query
        default: ""
    },
    file: {
        type: String, //url
        default: ""
    }
},{
    timestamps: true
});

questionModel.plugin(mongoosePaginate)
questionModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('question', questionModel);