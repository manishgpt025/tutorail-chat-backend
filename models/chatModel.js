const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let chatModel = new schema({
    room_id: {
        type: String,// room id for chat
        trim: true,
    },
    sender_id: {
        type: schema.Types.ObjectId,
        ref:"user"
    },
    receiver_id: {
        type: schema.Types.ObjectId,
        ref:"user"
    },
    message: {
        type: String, // message of person
        default: ""
    },
    status: {
        type: Boolean, //message status
        default: 0
    }
},{
    timestamps: true
});

chatModel.plugin(mongoosePaginate)
chatModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('chat', chatModel);