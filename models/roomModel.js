const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const mongooseAggregatePaginate = require('mongoose-aggregate-paginate');
const schema = mongoose.Schema;

let roomModel = new schema({
    room_id: {
        type: String, //room id for chat
        trim: true,
    },
    sender_id: {
        type: schema.Types.ObjectId,
        ref:"user"
    },
    receiver_id: {
         type: schema.Types.ObjectId,
         ref:"user"
    },
    status: {
        type: Boolean, //User status
        default: 1
    }
},{
    timestamps: true
});

roomModel.plugin(mongoosePaginate)
roomModel.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('room', roomModel);