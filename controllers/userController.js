const userService = require('../services/userService.js');// user services
const chatService = require('../services/chatService.js');// chat services
const roomService = require('../services/roomService.js');// room services
const questionService = require('../services/questionService.js');// question services
const helper = require('../globalFunctions/function.js');// helper and glocal functions
const responseHandle = require('../globalFunctions/responseHandle.js');
const responseCode = require('../globalFunctions/httpResponseCode.js');
const bcrypt = require('bcryptjs');
const salt = bcrypt.genSaltSync();
const ObjectId = require('mongodb').ObjectID;
const multiparty = require('multiparty');
// JWT
const jwt = require('jsonwebtoken');
const secret ='tutorial';
const nodemailer = require('nodemailer');
const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: "tutorial191",
    api_key: "165441486259728",
    api_secret: "lvl583zxh3AvKnfGZ-J0037dnQE"
});
/**
 * [Login API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const login = async (req, res) => {
    let check = helper.checkRequest(["email", "password"], req.body);
    if (check == true) {
        userService.findOneData({ email: req.body.email.toLowerCase()},{}, (error, userInfo) => {

            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                bcrypt.compare(req.body.password, userInfo.password, async (errorPassword, result) => {
                    if (result) {
                        let query = {
                            "_id":userInfo._id
                        };
                        let userData = await userService.findOneAsync(query,{password:0});
                        // JWT
                        let token = jwt.sign({ email: userData.email},
                            secret, {
                                expiresIn: '24h' // expires in 24 hours
                            }
                        );
                        userData.token = token;
                       return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Login successfully', userData);
                    } else {
                        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Credentials not match');
                    }
                });
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Email Id not found.');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [signUp API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const signUp = async (req, res) => {
    let check = helper.checkRequest(["name","email", "password"], req.body);
    if (check == true) {
        userService.findOneData({ email: req.body.email.toLowerCase()},{}, (error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Email Id already registered.');
            }else{
                let password = bcrypt.hashSync(req.body.password, salt);
                let insertData = {
                    "name": req.body.name,
                    "email": req.body.email.toLowerCase(),
                    "password": password
                };
                userService.createData(insertData, async (createError, createSuccess) => {
                    if (createError)
                        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
                    else{
                        // JWT
                        let token = jwt.sign({ email: createSuccess.email},
                            secret, {
                                expiresIn: '24h' // expires in 24 hours
                            }
                        );
                        let query = {
                            "_id":createSuccess._id
                        };
                        let userData = await userService.findOneAsync(query,{password:0});
                        userData.token = token;
                        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'SignUp successfully', userData);
                    }
                });
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [userCheckRoom API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const chatHistory = async (req, res) => {
    let check = helper.checkRequest(["senderId", "receiverId"], req.body);
    if (check == true) {
        userService.findOneData({ _id: req.body.senderId},{}, async(error, userInfo) => {
            if (error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
            else if (userInfo) {
                let queryData = {
                        $or: [
                          { $and: [{sender_id: req.body.senderId}, {receiver_id: req.body.receiverId}] },
                          { $and: [{sender_id: req.body.receiverId}, {receiver_id: req.body.senderId}] }
                        ]
                };
                let checkData = await roomService.findOneAsync(queryData,{});
                if(checkData){
                     let roomId = checkData.room_id;
                     let query={
                            room_id : roomId
                        };
                        let options = {message:1,room_id:1,sender_id:1,receiver_id:1,createdAt:1,status:1}
                        chatService.getData(query ,options , (errorData, successData) =>{
                            if (errorData)
                                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong',error);
                            else{
                                if(successData.length){
                                    let data={
                                        chatData:successData,
                                        roomId:checkData.room_id
                                    }
                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'conversation found', data);
                                }
                                else{
                                    let createRoomId = '';
                                    if(userInfo.role == 'user')
                                       createRoomId = successData.sender_id+successData.reciever_id;
                                    else
                                        createRoomId = successData.reciever_id+successData.sender_id;
                                    let data={
                                        chatData:[],
                                        roomId:createRoomId
                                    }
                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'conversation found', data);
                                }
                            }
                        });
                }else{
                    console.log(userInfo.role);
                    let createRoomId = '';
                    if(userInfo.role == 'user')
                        createRoomId = req.body.senderId+req.body.receiverId;
                    else
                        createRoomId = req.body.receiverId+req.body.senderId;
                    let data={
                        chatData:[],
                        roomId:createRoomId
                    }
                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'conversation found', data);
                }
            }else{
                return responseHandle.sendResponsewithError(res, responseCode.UNAUTHORIZED, 'Email Id not found.');
            }
        });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};

/**
 * [users API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const restUserList = async (req, res) => {
    userService.getData({role:'user'},{}, (error, userInfo) => {
    if (error)
        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
    else if(userInfo)
        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User List', userInfo);
    else
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Not found.');
    });
};

/**
 * [users API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const userList = async (req, res) => {
    let adminData = await userService.findOneAsync({'role':'admin'},{});
    let bodyData = {
          adminId: ObjectId(adminData._id)
    }
    roomService.getChatListData(bodyData, (error, userInfo) => {
    if (error)
        return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong');
    else if(userInfo){

        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User List', userInfo);
    }
    else
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Not found.');
    });
};

/**
 * [admin API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const adminData = async (req, res) => {
    let adminData = await userService.findOneAsync({'role':'admin'},{});
    if (adminData)
        return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'User List', adminData);
    else
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Not found.');
};

/**
 * [Contact API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const contactUs = async (req, res) => {
    let check = helper.checkRequest(["name","email","mobile","subject","message"], req.body);
    if (check == true) {
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
               user: 'apptutorial91@gmail.com',
               pass: 'tutorial123'
            }
        });
        let htmlData = `<p>Name: ${req.body.name}</p>
                        <p>Email: ${req.body.email}</p>
                        <p>Mobile Number: ${req.body.mobile}</p>
                        <p>Subject: ${req.body.subject}</p>
                        <p>Message: ${req.body.message}</p>`
        let mailOptions = {
            from: '<global.exam.champions@gmail.com>',
            to: 'global.exam.champions@gmail.com',
            subject: 'Contact Us',
            html: htmlData
        };
         transporter.sendMail(mailOptions, (error, info) => {
            if(error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went worng.',error);
            else
                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Thanks for contact us.',info);
         });
    } else {
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, `${check} key is missing.`);
    }
};


/**
 * [career API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const career = async (req, res) => {
    //let check = helper.checkRequest(["name","email","mobile","department","message","file"], req.body);
    let form = new multiparty.Form();
    form.parse(req, (error, fields, files) => {
         if (error) {
            console.log("err", error);
        } else {
            let basicData = fields;
            let fileData = files;
            console.log('1111111111111111111111',basicData)
            console.log('2222222222222222222222',fileData)
            if(
                !basicData.name
                    ||
                !basicData.email
                    ||
                !basicData.mobile
                    ||
                !basicData.department
                    ||
               ! basicData.message
            )
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide all detail.');
            else{
                if(Object.keys(fileData).length != 0){
                    console.log('33333333333333333333333333333',fileData.file[0].path)
                    cloudinary.v2.uploader.upload(fileData.file[0].path,
                        // { public_id: "sample_doc.docx",
                        // resource_type: "raw",
                        // },
                        (errorUpload, successUpload) => {
                        if(errorUpload)
                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went worng.',errorUpload);
                        else{
                            console.log('55555555555555555555555',successUpload)
                            const transporter = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    user: 'apptutorial91@gmail.com',
                                    pass: 'tutorial123'
                                }
                            });
                           let htmlData = `<p>Name: ${basicData.name[0]}</p>
                                <p>Email: ${basicData.email[0]}</p>
                                <p>Mobile Number: ${basicData.mobile[0]}</p>
                                <p>Department: ${basicData.department[0]}</p>
                                <p>Message: ${basicData.message[0]}</p>`
                            let mailOptions = {
                                from: '<global.exam.champions@gmail.com>',
                                to: 'global.exam.champions@gmail.com',
                                //to: basicData.email[0],
                                subject: 'Contact Us',
                                html: htmlData,
                                attachments: [{
                                    // use URL as an attachment
                                    filename: 'resume',
                                    path: successUpload.secure_url,
                                }]
                            };
                            console.log('4444444444444444444444444444',mailOptions);
                            transporter.sendMail(mailOptions, (errorSend, info) => {
                                if(errorSend)
                                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went worng.',errorSend);
                                else
                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Thanks for connecting us.',info);
                            });
                        }
                    });

                }else{
                    const transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'apptutorial91@gmail.com',
                            pass: 'tutorial123'
                        }
                    });
                   let htmlData = `<p>Name: ${basicData.name[0]}</p>
                        <p>Email: ${basicData.email[0]}</p>
                        <p>Mobile Number: ${basicData.mobile[0]}</p>
                        <p>Department: ${basicData.department[0]}</p>
                        <p>Message: ${basicData.message[0]}</p>`
                    let mailOptions = {
                        from: '<global.exam.champions@gmail.com>',
                        to: 'global.exam.champions@gmail.com',
                        //to: basicData.email[0],
                        subject: 'Contact Us',
                        html: htmlData
                    };
                    transporter.sendMail(mailOptions, (errorSend, info) => {
                        if(errorSend)
                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went worng.',errorSend);
                        else
                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Thanks for connecting us.',info);
                     });
                }
            }
        }
    });
};

/**
 * [question API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const question = async (req, res) => {
    let form = new multiparty.Form();
    form.parse(req, (error, fields, files) => {
         if (error) {
            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong.',error);
        } else {
            let basicData = fields;
            let fileData = files;
            console.log('1111111111111111111111',basicData)
            console.log('2222222222222222222222',fileData)
            if(
                !basicData.subject
                    ||
                !basicData.user_id
                    ||
                !basicData.question
            )
                return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Please provide all detail.');
            else{
                if(fileData){
                    cloudinary.v2.uploader.upload(fileData.file[0].path,
                        (errorUpload, successUpload) => {
                        if(errorUpload)
                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong.',errorUpload);
                        else{
                             let insertData = {
                                'subject':basicData.subject[0],
                                'user_id':basicData.user_id[0],
                                'question':basicData.question[0],
                                'file':successUpload.secure_url
                            }
                            questionService.createData(insertData, (errorInsert, result) => {
                                if(errorInsert)
                                    return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went worng.',errorInsert);
                                else
                                    return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Your query has been submitted successfully.',result);
                             });
                        }
                    });

                }else{
                    let insertData = {
                        'subject':basicData.subject,
                        'user_id':basicData.user_id,
                        'question':basicData.question
                    }
                    questionService.createData(insertData, (errorInsert, result) => {
                        if(errorInsert)
                            return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went worng.',errorInsert);
                        else
                            return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'Your query has been submitted successfully.',result);
                     });
                }
            }
        }
    });
};

/**
 * [getQuestion API]
 * @param  {[type]} req [object received from the application.]
 * @param  {[type]} res [object to be sent as response from the api.]
 * @return {[type]}     [function call to return the appropriate response]
 */
const getQuestion = async (req, res) => {

    let adminData = await userService.findOneAsync({'role':'admin'},{});

    if (adminData){
        let bodyData = {
            sort: {
                createdAt: -1
            },
            // populate: {
            //     path: "user_id",
            //     select: 'name email'
            // }
        };
        questionService.getQuestionListData(bodyData,(error, success)=>{
            if(error)
                return responseHandle.sendResponsewithError(res, responseCode.INTERNAL_SERVER_ERROR, 'Something went wrong.',error);
            else
                return responseHandle.sendResponseWithData(res, responseCode.EVERYTHING_IS_OK, 'question list', success);
        });
    }
    else
        return responseHandle.sendResponsewithError(res, responseCode.NOT_FOUND, 'Not found.');
};
/* Export apis */
module.exports = {
    login,
    signUp,
    chatHistory,
    userList,
    adminData,
    contactUs,
    career,
    question,
    getQuestion
};

