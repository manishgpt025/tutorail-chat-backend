const express = require('express');
const mongoose = require('mongoose');
const app = express();
const fs = require('fs');
const cors = require('cors');
const https = require('https');
const bodyParser = require('body-parser')

app.use(cors());
app.use(bodyParser.urlencoded({ limit: '100mb', extended: true }));
app.use(bodyParser.json({limit: '100mb', extended: true}));
app.set('trust proxy', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

// mongoose.connect(`mongodb://127.0.0.1:27017/tutorialChat`, { useNewUrlParser: true }, (err, result) => {
//   if (err) {
//     console.log("Error in connecting with database")
//   }
//   else {
//     console.log('Mongoose connecting is setup successfully')
//   }
// });

mongoose.connect(`mongodb+srv://nodeappuser:QWaCvKuyb12340978hsTwTTnhi@nodejs-backend-cluster.eao3u.mongodb.net/tutorialChat?retryWrites=true&w=majority`, { useNewUrlParser: true }, (err, result) => {
  if (err) {
    console.log("Error in connecting with database")
  }
  else {
    console.log('Mongoose connecting is setup successfully')
  }
});

//===========================================//
// app.use(express.static(__dirname + '/public/dist/tutorial-app'));
// app.get('/*', function (req, res) {
//   res.sendFile(__dirname +'/public/dist/tutorial-app/index.html');
// });

////=======================================////

const server = require('http').createServer(app)
const route = require('./routes/userRoute.js');
const userService = require('./services/userService.js');// user services
const chatService = require('./services/chatService.js');// chat services
const roomService = require('./services/roomService.js');// room services

const ObjectId = require('mongodb').ObjectID;
const io = require('socket.io')(server);
process.on('uncaughtException', function (err) {
  console.log(err);
});






//==========================Request Console=======================//

// app.all("*", (req, resp, next) => {
//   let obj = {
//     Host: req.headers.host,
//     ContentType: req.headers['content-type'],
//     Url: req.originalUrl,
//     Method: req.method,
//     Query: req.query,
//     Body: req.body,
//     Parmas: req.params[0]
//   }
//   console.log("Common Request is===========>", [obj])
//   next();
// });

//app.get('/ask',(req, res) => res.send('ask page'));

//call route by api
app.use('/api', route);

//===========================Socket===============================//
var sockets = {};
module.exports.sockets = sockets;

io.on('connection', socket => {
  console.log('socket connected', socket.id);

  //======================Room Join====================================//

  socket.on('room join', msg => {
    console.log('room join==========================', msg.roomId);
    socket.join(msg.roomId, () => {
      io.to(msg.roomId).emit('room join', { status: true, roomId: msg.roomId });
    });
  });

  //======================Room Leave==================================//

  socket.on('room leave', msg => {
    console.log('room leave', msg.roomId);
    socket.leave(msg.roomId, () => {
      io.to(msg.roomId).emit('room leave', { status: true, roomId: msg.roomId });
    });
  })

  //======================Typing start================================//

  socket.on('typeIn', msg => {
    console.log('typeIn', msg.roomId);
    io.to(msg.roomId).emit('typeIn', { status: true, roomId: msg.roomId });
  })

  //======================Typing stop================================//

  socket.on('typeOut', msg => {
    console.log('typeOut', msg.roomId);
    io.to(msg.roomId).emit('typeOut', msg);
  })

  //=====================Message send================================//

  socket.on('message', (msg, callback) => {
    msg.createdAt = new Date().toISOString();
    msg.status=true
    io.to(msg.room_id).emit('message', msg);
    let roomQuery = {room_id:msg.room_id};
    roomService.findOneData(roomQuery,{},(errorFind, successFind)=>{
      if(successFind){
        console.log('roomFind===' ,successFind);
        let chat = {
            "room_id": msg.room_id,
            "sender_id": msg.sender_id,
            "receiver_id": msg.receiver_id,
            "message": msg.message,
        };
        chatService.createData(chat,(error, result) => {
          console.log("Chat data is==========>",error, result);
        });
      }else{
        let roomData = {
          "room_id": msg.room_id,
          "sender_id": msg.sender_id,
          "receiver_id": msg.receiver_id
        };
        roomService.createData(roomData,(errorInsert, resultInsert) => {
          console.log("room data is==========>",errorInsert, resultInsert);
          let chat = {
            "room_id": msg.room_id,
            "sender_id": msg.sender_id,
            "receiver_id": msg.receiver_id,
            "message": msg.message,
          };
            chatService.createData(chat,(error, result) => {
              console.log("Chat data is==========>",error, result);
            });
        });
      }
    });
  });

  //======================Users list================================//
  socket.on('userListData',() => {
      getAlluserList();
  });

  getAlluserList = async () =>{
    let adminData = await userService.findOneAsync({'role':'admin'},{});
    let bodyData = {
          adminId: adminData._id
    }
    roomService.getChatListData(bodyData, (error, userInfo) => {
      if(error)
       // io.to(socket.id).emit("userListData",  []);
        io.emit('userListData', []);
      else{
        //io.to(socket.id).emit("userListData",  userInfo);
       // socket.broadcast.emit('userListData', userInfo);
        io.emit('userListData', userInfo);
      }
    });
    // setTimeout(() => {
    //     getAlluserList();
    // }, 1000);
  }


  //======================Read message=================================//

  socket.on('readMessage', (msg, callback) => {
    console.log("Request for read message is==============================>",msg)
    chatService.updateOneData(
      {
        room_id:msg.roomId,
        receiver_id:msg.userId
      },
      {$set:{status:true}},
      {multi:true},(err,result)=>{
      console.log("Save status",result)
    });
  });

  //=========================Upload File Start===========================//

  socket.on('uploadFileStart', data => {

    console.log("Data is==========>", data);
    var fileName = data[0].Name;
    var fileSize = data[0].Size;
    var Place = 0;
    var directory = '/var/www/html/erxNode-version/public/'
    if (fs.existsSync(directory)) {

    } else {
      fs.mkdir(directory)
    }
    var uploadFilePath = directory + '/' + fileName;
    console.log('uploadFileStart # Uploading file: %s to %s. Complete file size: %d', fileName, uploadFilePath, fileSize);
    Files[fileName] = {
      FileSize: fileSize,
      Data: "",
      Downloaded: 0
    }
    fs.open(uploadFilePath, "a", 0755, function (err, fd) {
      if (err) {
        console.log(err);
      }
      else {
        console.log('uploadFileStart # Requesting Place: %d Percent %d', Place, 0);
        Files[fileName]['Handler'] = fd;
        socket.emit('uploadFileMoreDataReq', { 'Place': Place, 'Percent': 0 });
      }
    });
  });

  //========================Upload file chunk========================//

  socket.on('uploadFileChuncks', data => {

    console.log("Upload request is============>", data);
    console.log(data[0].flag)
    var Name = data[0].Name;
    var base64Data = data[0].Data;
    var playload = new Buffer(base64Data, 'base64').toString('binary');
    console.log('uploadFileChuncks # Got name: %s, received chunk size %d.', Name, playload, playload.length);
    if (data[0].flag == 'new') {
      Files[Name]['Downloaded'] = playload.length;
      Files[Name]['Data'] = playload;
    } else {
      Files[Name]['Downloaded'] += playload.length;
      Files[Name]['Data'] += playload;
    }
    if (Files[Name]['Downloaded'] == Files[Name]['FileSize']) {
      console.log('uploadFileChuncks # File %s receive completed', Name);
      fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', (err, Writen) => {
        fs.close(Files[Name]['Handler'], () => {
          console.log('file closed');
        });
        var message = data[0];
        message.roomId = data[0].roomId;
        message.senderId = data[0].senderId;
        message.receiverId = data[0].receiverId;
        message.messageType = "Media";
        message.createdAt = new Date().toISOString();
        message.media = 'http://e-rx.cc/erxNode-version/public/' + Name
        console.log('file complete')
        console.log(message);
        socket.emit('uploadFileCompleteRes', { 'IsSuccess': true });
        io.to(data[0].roomId).emit('message', message);
        cloudinary.v2.uploader.upload('/var/www/html/erxNode-version/public/' + Name, { resource_type: "auto" }, (err1, result1) => {
          if (err1) {
            console.log("Err 1 is============>", err1)
          }
          else if (result1) {
            console.log("Url is==========>", result1.secure_url);
            let mediaObj = new ChatHistory({
              roomId: data[0].roomId,
              senderId: data[0].senderId,
              receiverId: data[0].receiverId,
              "messageType": "Media",
              "media": result1.secure_url,
            })
            mediaObj.save((error1, result2) => {
              console.log("Media chat data saved===========>", result2);
            })
          }
        })
      });
    }
    else if (Files[Name]['Data'].length > 10485760) {
      console.log('uploadFileChuncks # Updating file %s with received data', Name);
      fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', (err, Writen) => {
        Files[Name]['Data'] = "";
        var Place = Files[Name]['Downloaded'];
        var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
        socket.emit('uploadFileMoreDataReq', { 'Place': Place, 'Percent': Percent });
      });
    }
    else {
      var Place = Files[Name]['Downloaded'];
      var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
      console.log('uploadFileChuncks # Requesting Place: %d, Percent %s', Place, Percent);
      socket.emit('uploadFileMoreDataReq', { 'Place': Place, 'Percent': Percent });
    }
  });
});

//===============================================//
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization');

//  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

  // Pass to next layer of middleware
  next();
});

//===============================================================//

server.listen(process.env.PORT || 5000, () => {
  console.log(`tutorialChat listening on port 5000`);
});